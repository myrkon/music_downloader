pub struct AuthData {
    pub access_token: String,
    pub user_id: String
}

pub struct VkResponse<T> {
    pub raw_response: String,
    pub response: T
}

pub struct PlaylistModel {
    pub title: String,
    pub description: String,
    pub id: String,
    pub owner_id: String,
    pub cover_url: String,
    pub items: Vec<SongModel>
}

pub struct SongModel {
    pub artist: String,
    pub title: String,
    pub album_name: String,
    pub album_pic_url: String,
    pub id: String,
    pub access_key: String,
    pub owner_id: String,
    pub duration: i64
}

#[allow(dead_code)]
pub mod settings {
    const TOKEN_PATH: &str = "access_token"; 
}

pub mod request {
    use std::error::Error;
    pub fn get(url: String) -> Result<String, Box<dyn Error>> {
        let response = reqwest::blocking::get(&url)?;
        let json = response.text().unwrap();
        Ok(json)
    }

    pub fn get_file(url: String) -> Result<bytes::Bytes, Box<dyn Error>> {
        let client = reqwest::blocking::Client::builder()
            .timeout(None).build()?;
        let response = client.get(&url).send()?;
        let bytes = response.bytes().unwrap();
        let test: Vec<u8> = bytes[..].to_vec();
        Ok(bytes::Bytes::from(test))
    }
}

pub fn str2obj(str_obj: &String) -> Result<serde_json::Value, 
    Box<dyn std::error::Error>> {
    let obj = str_obj.parse().unwrap();
    Ok(obj)
}


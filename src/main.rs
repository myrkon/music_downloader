mod http;
mod vk;
mod data;

use clap::Clap;
use std::io::Write;


const VERSION: &'static str = env!("CARGO_PKG_VERSION");
// const PROGRAM_NAME: &'static str = env!("CARGO_PKG_NAME");
const AUTHOR: &'static str = env!("CARGO_PKG_AUTHORS");

#[derive(Clap)]
#[clap(version = VERSION, author = AUTHOR, about = "vk audio downloader.")]
struct Opts {
    #[clap(short = 'l', long = "list-playlists", about = "download tracks from playlist")]
    list_playlists: bool,

    #[clap(short = 'i', long = "playlist-id", about = "choose target playlist")]
    playlist_id: Option<i32>,

    #[clap(short = 'a', long = "auth-file", about = "choose file with auth data. it should contain access token in first line, and user id in second. Else I will return error.")]
    auth_file: Option<String>,

    #[clap(short = 't', long = "access-token")]
    access_token: Option<String>,

    #[clap(short = 'u', long = "user-id")]
    user_id: Option<String>,
}

fn main() {
    let opts: Opts = Opts::parse();

    let auth_d: data::AuthData;

    let auth_file = opts.auth_file.unwrap_or("".to_string());
    let mut access_token = opts.access_token.unwrap_or("".to_string());
    let mut user_id = opts.user_id.unwrap_or("".to_string());
    let playlist_id = opts.playlist_id.unwrap_or(-1);

    if auth_file != "".to_string() {
        let buffer: String= std::fs::read_to_string(auth_file)
            .expect("cannot read file");

        let file_contents: Vec<&str> = buffer.split("\n").collect();
        auth_d = data::AuthData {
            access_token: file_contents[0].to_string(),
            user_id: file_contents[1].to_string(),
        };
    } else if !access_token.is_empty() && !user_id.is_empty() {
        auth_d = data::AuthData {
            access_token: access_token,
            user_id: user_id
        };
    } else {
        println!("ERR: you don't provided auth data.");
        println!("open in browser next link: ");
        println!("https://oauth.vk.com/authorize?client_id=6121396&scope=1073737727&redirect_uri=https://oauth.vk.com/blank.html&display=page&response_type=token&revoke=1");
        println!("then log in, and insert url of blank page here:");

        let _ = std::io::stdout().flush();
        let mut url: String = String::new();
        std::io::stdin().read_line(&mut url).expect("you should enter a string");

        let offset = url.find('#').expect("invalid_url");
        let _: String = url.drain(..offset).collect();
        let parameters: Vec<&str> = url.char_indices()
            .next()
            .and_then(|(i, _)| url.get(i + 1..))
            .unwrap_or("")
            .split('&').collect();

        
        for parameter in parameters {
            let parts: Vec<&str> = parameter.split('=').collect();
            if parts[0] == "access_token".to_string() {
                access_token = parts[1].to_string();
            } else if parts[0] == "user_id".to_string() {
                user_id = parts[1].to_string();
            }
        }

        auth_d = data::AuthData {
            access_token: access_token,
            user_id: user_id
        };
    }


    let api = vk::Api::new(auth_d);
    if opts.list_playlists {
        let pls = api.get_playlists();
        for idx in 0..pls.len()-1 {
            println!("{} - {}", idx, pls[idx].title);
            let _ = std::io::stdout().flush();
        }
    } else if !opts.list_playlists && playlist_id != -1 {
        let pls = api.get_playlists();
        api.download_tracks_playlist(&api.get_playlist(0, &pls[playlist_id as usize]));
    } else {
        api.download_tracks_all_musics();
    }
}

use crate::data;
use crate::http;

use std::error::Error;

use std::fs::File;
use std::io::prelude::*;

use http::{request::*, str2obj};

use serde_json::Value;

#[allow(dead_code)]
pub struct Api {
    auth_d: data::AuthData
}

#[allow(dead_code)]
impl Api {
    pub fn new(auth_d: data::AuthData) -> Self {
        Self {
            auth_d: auth_d
        }
    }

    pub fn call(&self, method: String, args: String) 
        -> Result<data::VkResponse<Value>, Box<dyn Error>>  {
            let response = get(
                format!("http://api.vk.com/method/{}?{}\
                        &access_token={}&v=5.126", 
                        method, args, 
                        self.auth_d.access_token)
            )?; // TODO: fix this
            let vkobj = data::VkResponse::<Value> {
                response: str2obj(&response)?,
                raw_response: response
            };
            Ok(vkobj)
    }

    pub fn download_tracks_all_musics(&self) {
        println!("getting music count...");
        let count = geti2s(&self.call(s("audio.getCount"), format!("owner_id={}", &self.auth_d.user_id))
             .unwrap()
             .response["response"]);
        println!("done. Total: {} tracks.", count);
        println!("getting list of tracks...");
        let songs = self.call("audio.get".to_string(), format!("count={}", count)).unwrap().response;
        println!("done.");
        self.download_tracks(&self.json2playlist_model(&songs["response"]));
    }

    pub fn download_tracks_playlist(&self, playlist: &data::PlaylistModel) {
        println!("Playlist title: {}", playlist.title);
        if playlist.description != "".to_string() {
            println!("Description: {}", playlist.description);
        }
        self.download_tracks(playlist); 
    }

    pub fn get_playlists(&self) -> Vec<data::PlaylistModel> {
        let response = self.call("audio.getPlaylists".to_string(),
             format!("owner_id={}&count=100", self.auth_d.user_id)
        ).unwrap().response;

        self.json2pllist(&response["response"]["items"])
    }

    pub fn get_playlist(&self, offset: u8, playlist: &data::PlaylistModel) -> data::PlaylistModel {
        let mut playlist_model = self.json2playlist_model(
            &self.call("audio.get".to_string(), 
                       format!("offset={}&owner_id={}&album_id={}&count=100", 
                               offset, playlist.owner_id, playlist.id)
                       ).unwrap().response["response"]);
        playlist_model.title = sp2n(&playlist.title);
        playlist_model.description = sp2n(&playlist_model.description);
        playlist_model
    }
    
    pub fn json2song_model(&self, song_json: &Value) -> data::SongModel {
        let album_name;
        let album_pic_url;
        let access_key;
        let owner_id;

        if !song_json["album"].is_null(){ album_name = getstr(&song_json["album"]["title"]); } else { album_name = "".to_string(); }
        if !song_json["album"]["thumb"]["photo_1200"].is_null() { album_pic_url = getstr(&song_json["album"]["thumb"]["photo_1200"]); } else { album_pic_url = "".to_string(); }
        if !song_json["access_key"].is_null() { access_key = song_json["access_key"].as_str().unwrap().to_string(); } else { access_key = "".to_string(); }
        if !song_json["owner_id"].is_null() { owner_id = geti2s(&song_json["owner_id"]); } else { owner_id = s(""); }

        data::SongModel {
            artist: getstr(&song_json["artist"]),
            title: getstr(&song_json["title"]),
            album_name: album_name.to_string(),
            album_pic_url: album_pic_url.to_string(),
            id: geti2s(&song_json["id"]),
            access_key: access_key,
            owner_id: owner_id,
            duration: song_json["duration"].as_i64().unwrap()
        }
    }

    pub fn json2playlist_model(&self, playlist: &Value) -> data::PlaylistModel {
        let mut items = Vec::<data::SongModel>::new();
        let owner_id; let title; let description; let id; let cover_url;

        if !playlist["plid"]["thumbs"].is_null() { cover_url = getstr(&playlist["thumbs"][0]["photo_1200"]) }
        else if !playlist["photo"].is_null() { cover_url = getstr(&playlist["photo"]["photo_1200"]) } else { cover_url = "".to_string(); } 
        if !playlist["items"].is_null() { for item in playlist["items"].as_array().unwrap() { items.push(self.json2song_model(item)); } }
        if !playlist["owner_id"].is_null() { owner_id = geti2s(&playlist["owner_id"]); } else { owner_id = sp2n(&self.auth_d.user_id); }
        if !playlist["title"].is_null() { title = getstr(&playlist["title"]); } else { title = s("All Music"); }
        if !playlist["description"].is_null() { description = getstr(&playlist["description"]) } else { description = "".to_string(); }
        if !playlist["id"].is_null() { id = geti2s(&playlist["id"]); } else { id = sp2n(&self.auth_d.user_id); }

        data::PlaylistModel {
            title: title,
            description: description,
            id: id,
            owner_id: owner_id,
            cover_url: cover_url,
            items: items
        }
    }

    pub fn json2pllist(&self, json: &Value) -> Vec<data::PlaylistModel> {
        let pllist_json = &json;
        let pl_len = pllist_json.as_array().unwrap().len();
        let mut pllist = Vec::new();

        for plid in 0..pl_len-1 {
            pllist.push(self.json2playlist_model(&json[plid]));
        }

        pllist
    }

    fn download_tracks(&self, songs: &data::PlaylistModel) {
        for item in &songs.items {
            let song_name : &String = &format!("{} - {}.mp3", item.artist, item.title);
            println!("Downloading '{}'", song_name);
            let song_dir = format!("{}/{}", songs.title, 
                                   if item.album_name != "".to_string() { 
                                       format!("{}/{}", item.artist, item.album_name)
                                    } else {item.artist.to_string()});
            let song_path : &String = &format!("{}/{}", song_dir, song_name);
            let _ = std::fs::create_dir_all(&song_dir);

            let response = self.call("audio.getById".to_string(), 
                                     format!("audios={}_{}_{}",
                                             item.owner_id,
                                             item.id,
                                             item.access_key)).unwrap().response;

            let song_url : &String = &getstr(&response["response"][0]["url"]);
            let song_bytes : bytes::Bytes = bytes::Bytes::from(get_file(song_url.to_string()).unwrap());
            println!("song was downloaded.");
            
            let mut file = File::create(song_path).unwrap();
            let _ = file.write_all(&song_bytes);

            println!("editing tags...");
            let mut song_tag = match id3::Tag::read_from_path(song_path) {
                Ok(n) => n,
                Err(_) => {println!("ERR: tags assignment failed, skipping."); continue}
            };
            song_tag.set_artist(&item.artist);
            song_tag.set_title(&item.title);
            if item.album_name != "".to_string() {
                println!("album data was founded, adding it...");

                let album_pic_bytes : bytes::Bytes = bytes::Bytes::from(get_file(item.album_pic_url.to_string()).unwrap());

                song_tag.set_album(&item.album_name);
                song_tag.add_picture(id3::frame::Picture { 
                    mime_type: "image/jpeg".to_string(),
                    picture_type: id3::frame::PictureType::Other,
                    description: "".to_string(),
                    data: album_pic_bytes.to_vec()
                });
            }
            let _ = song_tag.write_to_path(song_path, id3::Version::Id3v24);
            println!("done.");
        }
    }
}

fn getstr(value: &Value) -> String { value.as_str().unwrap().to_string() }
fn geti2s(value: &Value) -> String { value.as_i64().unwrap().to_string() } 
fn sp2n(value: &String) -> String { (&value).to_string() }
fn s(value: &str) -> String { value.to_string() }

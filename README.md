music_downloader
---

Это маленькая программа, написаная на расте, с помощью которой можно скачивать музыку с вк. 

Писал я её чисто в целях изучения раста.

### Компилирование

``cargo build``

бинарник будет в ``target/debug``.

Если надо уменьшить размер, то компилировать надо в Release:

- ``cargo build --release``
- ``strip target/release/music_downloader``

### Использование:

Для работы программы, нужны данные авторизации вк. А точнее:

- access_token
- user_id

Получить их можно открыв в браузере ссылку авторизации:

``https://oauth.vk.com/authorize?client_id=6121396&scope=1073737727&redirect_uri=https://oauth.vk.com/blank.html&display=page&response_type=token&revoke=1``

После авторизации, вы попадете на пустую страницу. Из её url копируйте оттуда access_token и user_id.

#### Подсказка

пример ссылки:

``https://oauth.vk.com/blank.html#access_token=XXXX&expires_in=0&user_id=123123123&email=example@mail``

все, что до решетки - нам не нужно, это можно удалить. 

``access_token=XXXX&expires_in=0&user_id=123123123&email=example@mail``

амперсант - это разделитель, его можно убрать, заменив например \n.

``access_token=XXXX``

``expires_in=0``

``user_id=123123123``

``email=example@mail``

``expires_in`` и ``email`` нам не нужны

#### Файл авторизации

Его синтаксис довольно прост.

в первой строке должно находится ЗНАЧЕНИЕ переменной access_token.
во второй строке должно находится ЗНАЧЕНИЕ переменной user_id.
